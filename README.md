# 1. Local Composer Repository
Downloading packages from [packagist.org][13] can be extermely slow in some
regions on this planet, therefore it would be very helpful if the developers in
those places could host the packages they need on a local server. [Satis][9] is
a static composer repository generator that can be used to host packages
locally, however, this setup still does not resolve the network latency between
`packagist.org` and the local satis server. 

To overcome this problem, this tutorial introduces an approach that uses a
combination of [AWS][1] [EC2][14] and [S3][15] services as a bridge between
local satis server and `packagist.org`: a remote satis server is deployed on an
AWS EC2 instance, and the packages it downloads from `packagist.org` are hosted
on a S3 bucket; the S3 bucket has been configured to support [static website
hosting][9]; the local satis server, therefore, synchronizes itself with the
static website over HTTP protocol; when being initialized, the local stais
server may synchronize the packages through the [AWS CLI][7], since it is much
faster than over the static website; the composer on local machine installs
packages from the local satis server.

# 2. Configure Remote Satis Server

## 2.1 Register an AWS Account
1. Register an account at [aws.amazon.com][1];
1. Follow the instructions to enable services such as `ec2` and `s3`;
1. Access [AWS console][2] to manage all services.

## 2.2 Configure EC2 Instance

### 2.2.1 Create Instance
1. Launch an `ubuntu 64-bit` ec2 instance at [AWS console][2];
1. You will be asked to create a security key the first time you launch an `ec2`
   instance; download the the key to your desktop;
1. Check and rememer the public ip address of the `ec2` instance;
1. Update the firewall rules in `NETWORK & SECURITY` >> `Security Groups`:
    1. Inbound TCP 22 
    1. ...

### 2.2.2 Configure SSH Connection
1. Configure SSH client on local machine:

```bash
$ # Download AWS EC2 access key:
$ cp <path-to-the-downloaded-key> ~/.ssh/<key-name>.pem
$ chmod 400 ~/.ssh/<key-name>.pem
$ 
$ # Create ssh config file:
$ cat << EOF >> ~/.ssh/config
> Host <alias-of-ec2>
> HostName <ip-address-of-the-instance>
> User ubuntu
> IdentityFile ~/.ssh/<key-name>.pem
> # Configure dynamic forwarding to allow socket proxy:
> DynamicForward localhost:3128
> Protocol 2
> ServerAliveInterval 60
> EOF
```

2. Test the connection to EC2:

```bash
$ ssh <alias-of-ec2>
```

## 2.3 Configure S3 Bucket

### 2.3.1 Create Bucket
1. Create a s3 bucket ``satis.<your-domain-name>`` at [AWS Console][2]
1. Make it public as follows:
    1. Click permissions tab and choose ``Access Control List`` >> ``Public
       access`` >> ``Everyone`` >> ``List objects`` [x] ;
    1. Click permissions tab, choose ``Bucket Policy`` and use `Policy
       generator` to generate a policy statement as follows:

```json
{
    "Version": "yyyy-mm-dd",
        "Id": "<generated-policy-id>",
        "Statement": [
            {
            "Sid": "<generated-statement-id>",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::satis.<your-domain-name>/*"
            }
        ]
}
```

> The policy statement makes this bucket always public, otherwise, each time
> when new files have been uploaded to the bucket, you have to manually mark
> these files public.

### 2.3.2 Create IAM Account
1. Choose ``<user name>`` >> ``My Security Credentials`` from the top menu of
   [AWS Console][2];
1. A dialog shows up and asks you to choose between 
   <kbd>Continue to Security Credentials</kbd> and <kbd>Get Started with IAM Users</kbd>;
   click <kbd>Get Started with IAM Users</kbd>;
1. Click <kbd>Add User</kbd> to create a new IAM user for `programmatic access`;
1. At the step of setting permissions, create a new group with permission policy
   `AmazonS3FullAccess` and assign this group to the user being created;
1. After the user has been created, remember the `Access key ID` and `Secret
   access key` at the user's `Security credentials` page.

> The `Access key ID` and `Secret access key` are used to authenticate a client
> for accessing the s3 bucket. You can NOT retrieve the secrete once it has
> been created, but you can always create a new pair of `key id` and `secret`, 
> if you lost the the previous one.

### 2.3.4 Configure S3 Bucket for Static Website
1. Find the S3 bucket created at [AWS Console][2];
1. Click the `Properties` link to open the `Properties` tab of the S3 bucket;
1. Click the `Static website hosting` block to open the configuration page;
1. Choose `Use this bucket to host a website` [x]  and specify the names of index
   document and error document.

> Take a note of the `Endpoint` URL in this page, which is something like 
> 
> http://satis.\<your-domain\>.s3-website.\<mirror-id\>.amazonaws.com, that will
> be used to access the static website.

### 2.3.5 Mount S3 Bucket on EC2

```bash
$ # Install s3fs:
$ sudo apt-get install s3fs
$  
$ # Configure S3 credentials:
$ echo "<key-access-id>:<secret-access-key>" >> ~/.passwd-s3fs
$  
$ # Create mount point:
$ sudo mkdir /mnt/satis 
$ sudo chown ubuntu.ubuntu /mnt/satis
$  
$ # Mount S3 bucket:
$ s3fs -o allow_other,uid=1000,gid=1000,umask=027 satis.<your-domain> /mnt/satis
```

### 2.3.6 Test the Static Website
1. Create an index document:

```bash
$ # Create index document:
$ cd /mnt/satis
$ cat << EOF > index.html
> <!doctype html>
> <html lang="en_US">
>     <head>
>         <meta charset="utf-8" />
>     </head>
>     <body>
>        <h1>Welcome!</h1>
>     </body>
> </html>
> EOF
```

2. Access the website at the `Endpoint` URL:
   * http://satis.\<your-domain\>.s3-website.\<mirror-id\>.amazonaws.com

## 2.4 Install & Configure Satis

### 2.4.1 Install Composer

```bash
$ # Install composer:
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
$ sudo mv composer.phar /usr/local/bin/composer
```

### 2.4.2 Install Satis

```bash
$ # Create base directory:
$ mkdir ~/github
$ cd ~/github
$  
$ # Install satis:
$ composer create-project composer/satis:dev-master --keep-vcs
$ sudo ln -s satis/bin/satis /usr/local/bin/satis
```

### 2.4.3 Configure Satis

```bash
$ # Create server base directory:
$ mkdir ~/satis.server
$ cd ~/satis.server
$  
$ # Initialize directory structure on S3 bucket:
$ mkdir /mnt/satis/dist
$ mkdir /mnt/satis/include
$ touch /mnt/satis/index.html
$ touch /mnt/satis/packages.json
$ ln -s /mnt/satis/dist .
$ ln -s /mnt/satis/include .
$ ln -s /mnt/satis/index.html .
$ ln -s /mnt/satis/packages.json .
$ 
$ # Create configuration file:
$ cat << EOF > satis.json
> {
>     "name": "<name-of-the-satis-server>",
>     "homepage": "http://satis.<your-domain>.s3-website.<mirror-id>.amazonaws.com",
>     "require": {
>        ... any packages to be cached ...
>     },
>     "require-dependencies": true,
>     "require-dev-dependencies": false,
>     "archive": {
>        "directory": "dist",
>        "format": "tar",
>        "prefix-url": "http://satis.<your-domain>.s3-website.<mirror-id>.amazonaws.com",
>        "skip-dev": false,
>        "checksum": false,
>        "override-dist-type": true
>     }
>  }
> EOF
```
::: info-box note :::

Due to a [bug][16] in `ext/phar`, checksum must be disabled. Otherwise, tar
archvies generated at differnt time may have different checksums, even they
contain the same files.

:::

### 2.4.4 Build Satis

```bash
$ # Build satis server
$ satis build --skip-errors ~/satis.server/satis.json ~/satis.server
```

# 3. Configure Local Satis Server

## 3.1 Install & Configure Satis

### 3.1.1 Install Composer

```bash
$ # Install composer:
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"
$ sudo mv composer.phar /usr/local/bin/composer
```

### 3.1.2 Install Satis

```bash
$ # Create base directory:
$ mkdir ~/github
$ cd ~/github
$ 
$ # Install satis:
$ composer create-project composer/satis:dev-master --keep-vcs
$ sudo ln -s satis/bin/satis /usr/local/bin/satis
```

### 3.1.3 Configure Satis

```bash
$ # Create server base directory:
$ mkdir ~/satis.server
$ cd ~/satis.server
$ 
$ # Create configuration file:
$ cat << EOF > satis.json
> {
>     "name": "<name-of-the-satis-server-on-local-machine>",
>     "homepage": "http://satis.localhost:4680",
>     "repositories": [
>         { "type": "composer", "url": "http://satis.<your-domain>.s3-website.<mirror-id>.amazonaws.com" },
>     ],
>     "require": {
>         ... any packages to be cached ...
>     },
>     "config": {
>         "secure-http": false
>     },
>     "require-dependencies": true,
>     "require-dev-dependencies": false,
>     "archive": {
>         "directory": "dist",
>         "format": "tar",
>         "prefix-url": "http://satis.localhost:4680",
>         "skip-dev": false,
>         "checksum": false,
>         "override-dist-type": true
>     }
> }
> EOF
$  
$ # Configure nginx virtual host:
$ cat << EOF > ~/satis.localhost.conf
> server {
>     listen 4680;
>     root <path-to-satis.server>;
>     index index.html;
>     server_name satis.localhost;
> }
> EOF
$  
$ # Deploy the virtual host:
$ sudo cp ~/satis.localhost.conf <nginx-etc-directory>/sites-enabled
$  
$ # Start the virtual host:
$ sudo service nginx restart

$ # Map local hostname:
$ sudo tee -a /etc/hosts > /dev/null << EOF
> 127.0.0.1 satis.localhost
> EOF
```

### 3.1.4 Synchronize with Remote Server

#### 3.1.4.1 Install AWS CLI
1. Download the latest release of `Python` from [Python.org][1] and install it.
1. Install AWS CLI as follows:

```bash
$ # Install Python package manager:
$ curl -O https://bootstrap.pypa.io/get-pip.py
$ python3 get-pip.py --user
$  
$ # Update path:
$ echo "export ~/Library/Python/3.6/bin:\$PATH" >> ~/.profile
$ source ~/.profile
$  
$ # Configure AWS credentials:
$ mkdir ~/.aws
$ cat << EOF > ~/.aws/credentials
$ > [default]
$ > aws_access_key_id=<key-access-id>
$ > aws_secret_access_key=<secret-access-key>
$ > EOF
```

#### 3.1.4.2 Synchronize Remote Files

```bash
$ aws s3 sync --delete s3://satis.<your-domain-name>/dist <path-to-satis.server>/dist
$ aws s3 sync --delete s3://satis.<your-domain-name>/include <path-to-satis.server>/include
```

> Sychronizing remote directory to local directory:
>
> $ aws s3 sync --delete s3://satis.\<your-domain-name\> \<local-directory\>
>
> Synchronizing local directory to remote directory:
> 
> $ aws s3 sync --delete \<local-directory\> s3://satis.\<your-domain-name\>

### 3.1.5 Build Satis

```bash
$ satis build --skip-errors ~/satis.server/satis.json ~/satis.server
```

# 4. Configure Local Composer

# 4.1 Say Goodbye to Packagist

```bash
# Disable packagist:
$ composer config -g repo.packagist false

# Allow non-secure http connection:
$ composer config -g secure-http false

# Force to fetch packages from local satis server:
$ composer config -g repo.repository "composer" "http://satis.localhost:4680"
```

# 5. Let's Rock it! :sunglasses: 

```bash
$ mkdir ~/test
$ cd ~/test
$ # Try to install some packages and enjoy the speed :)
$ composer require symfony/symfony
```

# 6. Maintenance

## 6.1 Add New Packages
1. Add packages to `satis.json` on both local and remote servers;
1. Manually run `satis build` on remote server, or wait for the cronjob;
1. After the remote server has been updated, run `satis build` on local server,
   or leave it to the cronjob.

## 6.2 Keep Remote Satis Updated

```bash
# Update packages on daily basis
$ (export EDITOR=tee && (crontab -l ; echo "2 * * * * <path-to-php> /usr/local/bin/satis build --skip-errors <path-to-satis.server>/satis.json <path-to-satis.server>")  | uniq - | crontab -e)
```
## 6.3 Keep Local Satis Updated

```bash
# Update packages on daily basis
$ (export EDITOR=tee && (crontab -l ; echo "4 * * * * <path-to-php> /usr/local/bin/satis build --skip-errors <path-to-satis.server>/satis.json <path-to-satis.server>")  | uniq - | crontab -e)
```

> Downloading files through AWS CLI could be faster than HTTP, so if necessary,
> try to synchronize `dist` and `include` directories before building satis
> server.


# 7. Appendix

# 7.1 Configure Proxy for Firefox:
1. Open `Firefox`;
1. Choose `Firefox` >> `Preferences ...` >> `Network Proxy` >> `Settings ...`
1. Select `Manual proxy configuration` and config the settings as follows:

| Protocol   | Host          |              | Port     |
| ---------: | :------------ | -----------: | :------- |
| HTTP Proxy |               | Port         | 0        |
| SSL Proxy  |               | Port         | 0        |
| FTP Proxy  |               | Port         | 0        |
| SOCKS Host | **localhost** | Port         | **3128** |
|            |               |              |          |
|            | SOCKS v5      | [x] SOCKS v5 |          | 

| No Proxy for                                                                        |
|-------------------------------------------------------------------------------------|
| List domains and ip addresses, the traffic of which do not go through the EC2 proxy |

4. Restart `Firefox` and test. 


# 8. References

| A                                                           | B                                                      |
| ----------------------------------------------------------- | ------------------------------------------------------ |
| \[1\] [AWS Website][1]                                      |  \[2\] [AWS Console][2]                                |
| \[3\] [Install 3sfs][3]                                     |  \[4\] [Bash Heredoc][4]                               |
| \[5\] [SSH Dynamic Forwarding][5]                           |  \[6\] [AWS S3 Bucket Policy][6]                       |
| \[7\] [Configure Credentials for AWS CLI][7]                |  \[8\] [Using S3 Commands with AWS CLI][8]             |
| \[9\] [Configure S3 Bucket for Static Website Hosting][9]   |  \[10\] [Install Composer][10]                         |
| \[11\] [Install Satis][11]                                  |  \[12\] [Linux Sudo and Heredoc][12]                   |
| \[13\] [Packagist.org][13]                                  |  \[14\] [AWS EC2][14]                                  |
| \[15\] [AWS S3][15]                                         |  \[16\] [Phar PAX Format Issue][16]                                                  |


 [1]: http://aws.amazon.com "AWS"
 [2]: http://console.aws.amazon.com "AWS Console"
 [3]: https://cloud.netapp.com/blog/amazon-s3-as-a-file-system "Install 3sfs"
 [4]: https://stackoverflow.com/questions/2953081/how-can-i-write-a-heredoc-to-a-file-in-bash-script "Bash Heredoc"
 [5]: https://stackoverflow.com/questions/18704195/how-to-add-socks-proxy-to-ssh-config-file "SSH Dynamic Forward"
 [6]: https://docs.aws.amazon.com/AmazonS3/latest/dev/example-bucket-policies.html "AWS S3 Bucket Policy"
 [7]: https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html "Configure Credentials for AWS Cli"
 [8]: https://docs.aws.amazon.com/cli/latest/userguide/using-s3-commands.html "Using High-Level S3 Commands with AWS Cli"
 [9]: https://docs.aws.amazon.com/AmazonS3/latest/user-guide/static-website-hosting.html "Configure S3 Bucket for Static Website Hosting"
[10]: https://getcomposer.org/download/ "Install Composer"
[11]: https://github.com/composer/satis "Install Satis"
[12]: https://stackoverflow.com/questions/4412029/generate-script-in-bash-and-save-it-to-location-requiring-sudo "Linux Sudo and Heredoc"
[13]: https://packagist.org "Packagist.org"
[14]: https://aws.amazone.com/ec2/ "AWS EC2"
[15]: https://aws.amazon.com/s3/ "AWS S3"
[16]: https://bugs.php.net/bug.php?id=77229 "Phar PAX Format Issue"
